#### Check the current state of locally initialised repository
```
git status
On branch master
No commits yet
nothing to commit (create/copy files and use "git add" to track)
```
#### add new file `touch README.md`
#### Check the state again
```
git status
On branch master
No commits yet
Untracked files:
(use "git add <file>..." to include in what will be committed)
README.md
nothing added to commit but untracked files present (use "git add" to track)
```
#### add file to be tracked by local git repository `git add README.md`
#### Check the status again
```
git status
On branch master
No commits yet
Changes to be committed:
(use "git rm --cached <file>..." to unstage)
new file: README.md
```
#### Added few lines or some data in the `README.md`
#### Check the status again
```
git status
On branch master

No commits yet

Changes to be committed:
(use "git rm --cached <file>..." to unstage)
new file: README.md

Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git restore <file>..." to discard changes in working directory)
modified: README.md
```

new changes
one more changess

sssw
